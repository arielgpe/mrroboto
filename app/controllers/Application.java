package controllers;


import play.libs.F;
import play.libs.ws.WS;
import play.libs.ws.WSResponse;
import play.mvc.Controller;
import play.mvc.Result;

public class Application extends Controller {

    public static F.Promise<Result> index() throws Exception {

        F.Promise<Result> resultPromise = WS.url("https://slack.com/api/rtm.start?token=xoxb-4372014118-mFlT62bTobwySJ9A6M0NpcyK&pretty=1")
                .get().map(
                        new F.Function<WSResponse, Result>() {
                            public Result apply(WSResponse response) {
                                return ok(response.asJson().get("url"));
                            }
                        }
                );
        return resultPromise;
    }

}
