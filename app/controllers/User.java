package controllers;

import com.fasterxml.jackson.databind.JsonNode;
import models.Presence;
import models.Users;
import play.libs.Json;
import play.mvc.BodyParser;
import play.mvc.Controller;
import play.mvc.Result;

import java.util.List;

/**
 * Created by Ariel Guzman on 4/11/15
 */
public class User extends Controller {

    public static Result get_users(){
        List<Users> users = Users.findAll();
        return ok(Json.toJson(users));
    }

    public static Result get_user(String id){
        Users users = Users.findById(id);
        return ok(Json.toJson(users));
    }

    @BodyParser.Of(BodyParser.Json.class)
    public static Result post_user(){
        JsonNode json = request().body().asJson();
        String id = json.findPath("id").textValue();
        String name = json.findPath("name").textValue();
        String realName = json.findPath("realName").textValue();
        if (id.isEmpty() || name.isEmpty() || realName.isEmpty()){
            return badRequest("You are missing some parameters");
        } else {
            Users user = Users.create(id, name, realName);
            return ok(Json.toJson(user));
        }
    }

    public static Result get_presences(){
        List<Presence> presences = Presence.findAll();
        return ok(Json.toJson(presences));
    }

    public static Result get_presence(String userId){
        Presence presence = Presence.findByUserId(userId);
        return ok(Json.toJson(presence));
    }

    @BodyParser.Of(BodyParser.Json.class)
    public static Result update_presence(String userId){
        JsonNode json = request().body().asJson();
        String value = json.findPath("value").textValue();
        if(value == null) {
            return badRequest("Missing parameter [value]");
        } else {
            Presence presence = Presence.update(value, userId);
            return ok(Json.toJson(presence));
        }
    }
}
