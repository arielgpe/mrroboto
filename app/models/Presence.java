package models;

import play.db.ebean.Model;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.OneToOne;
import java.util.Date;
import java.util.List;

/**
 * Created by Ariel Guzman on 4/11/15
 */
@Entity
public class Presence extends Model {
    @Id
    public Long id;

    @OneToOne(cascade = CascadeType.ALL)
    public Users user;

    public String value;

    public Date updatedAt;

    private static Finder<Long, Presence> finder(){
        return new Finder<Long, Presence>(Long.class, Presence.class);
    }

    public static List<Presence> findAll(){
        return finder().findList();
    }

    public static Presence findByUserId(String userId){
        return finder().where().eq("user_id", userId).findUnique();
    }

    public static Presence create(Users user, String value){
        Presence presence = new Presence();
        presence.user = user;
        presence.value = value;
        presence.updatedAt = new Date();
        presence.save();
        return presence;
    }

    public static Presence update(String value, String userId){
        Presence presence = finder().where().eq("user_id", userId).findUnique();
        presence.updatedAt = new Date();
        presence.value = value;
        presence.update();
        return presence;
    }
}
