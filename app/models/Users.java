package models;

import com.fasterxml.jackson.annotation.JsonBackReference;
import play.db.ebean.Model;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.OneToOne;
import java.util.List;

/**
 * Created by Ariel Guzman on 4/11/15
 */
@Entity
public class Users extends Model {
    @Id
    public String id;

    public String name;

    public String realName;

    @OneToOne(mappedBy = "user")
    @JsonBackReference
    public Presence presence;

    private static Finder<String, Users> finder(){
        return new Finder<String, Users>(String.class, Users.class);
    }

    public static List<Users> findAll(){
        return finder().findList();
    }

    public static Users findById(String id){
        return finder().byId(id);
    }

    public static Users create(String id, String name, String realName){
        Users user = new Users();
        user.id = id;
        user.name = name;
        user.realName = realName;
        Presence.create(user, "away");
        return user;
    }
}
