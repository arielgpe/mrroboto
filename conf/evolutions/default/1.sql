# --- Created by Ebean DDL
# To stop Ebean DDL generation, remove this comment and start using Evolutions

# --- !Ups

create table presence (
  id                        bigint auto_increment not null,
  user_id                   varchar(255),
  value                     varchar(255),
  updated_at                datetime,
  constraint pk_presence primary key (id))
;

create table users (
  id                        varchar(255) not null,
  name                      varchar(255),
  real_name                 varchar(255),
  constraint pk_users primary key (id))
;

alter table presence add constraint fk_presence_user_1 foreign key (user_id) references users (id) on delete restrict on update restrict;
create index ix_presence_user_1 on presence (user_id);



# --- !Downs

SET FOREIGN_KEY_CHECKS=0;

drop table presence;

drop table users;

SET FOREIGN_KEY_CHECKS=1;

