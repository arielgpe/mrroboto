-- MySQL dump 10.15  Distrib 10.0.16-MariaDB, for Linux (x86_64)
--
-- Host: localhost    Database: roboto
-- ------------------------------------------------------
-- Server version	10.0.16-MariaDB-log
# --- !Ups

-- Dumping data for table `presence`
--
/*!40000 ALTER TABLE `users` DISABLE KEYS */;
INSERT INTO `users` VALUES ('U02A74D7V','arielgpe','Ariel Guzman'),('U04AUTYSQ','alfierivera','Alfredo Rivera'),('U04AV30TL','melvis','Melvis Perez'),('U04AV5R8W','sergio','Sergio Sandino'),('U04AV7WKW','kapzuba','Marcos Lebron'),('U04BHE36B','htoledo','Hector Toledo'),('U04BHFZDB','rockman','Roman Ferreiras'),('U04BHGUCZ','day','Jeiny Figueroa');
/*!40000 ALTER TABLE `users` ENABLE KEYS */;


/*!40000 ALTER TABLE `presence` DISABLE KEYS */;
INSERT INTO `presence` VALUES (1,'U02A74D7V','active','2015-04-12 12:30:27'),(2,'U04AUTYSQ','away','2015-04-11 22:26:58'),(3,'U04BHGUCZ','away','2015-04-11 19:12:00'),(4,'U04BHE36B','away','2015-04-11 19:13:04'),(5,'U04AV7WKW','away','2015-04-11 19:14:02'),(6,'U04AV30TL','away','2015-04-11 19:14:28'),(7,'U04BHFZDB','away','2015-04-11 19:15:59'),(8,'U04AV5R8W','away','2015-04-11 19:16:41');
/*!40000 ALTER TABLE `presence` ENABLE KEYS */;
--
-- Dumping data for table `users`
--

# --- !Downs
TRUNCATE TABLE `users`;
TRUNCATE TABLE `play_evolutions`;
TRUNCATE TABLE `presence`;